package uf1_examen1;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Endevina {

	public static void main(String[] args) {

		//El gerard m'ha ajudat a entendre la part de sortida d'intents que no havia ent�s de l'enunciat
		Scanner sc = new Scanner(System.in);
		Random r = new Random();

		System.out.println("Inserta un n�mero");
		int numUsuario = sc.nextInt();
		int num = r.nextInt(1000) + 1;
		int contador = 0;
		boolean flag = false;
		ArrayList<Integer> llista = new ArrayList<Integer>();
		
		while (numUsuario != num && contador < 15) {
			if (contador < 16) {
				if (num > numUsuario) {
					System.out.println("Cal un valor m�s gran");
					contador++;
					llista.add(numUsuario);
					numUsuario = sc.nextInt();

				} else if (num < numUsuario) {
					System.out.println("Cal un valor m�s petit");
					contador++;
					llista.add(numUsuario);
					numUsuario = sc.nextInt();
				}
			}
		}
		if (numUsuario == num) {
			System.out.println("Enhorabona, has encertat el n�mero!!!!");

		}

		if (contador >= 15) {
			System.out.println("Cal que estiguis m�s espavilat");
			System.out.println("Has fet " + contador + " intents");
			System.out.println("Els n�meros que has intentat s�n: ");
			System.out.println(llista);
			System.out.println("El n�mero era "+num);
		}
		
		
	}

}
