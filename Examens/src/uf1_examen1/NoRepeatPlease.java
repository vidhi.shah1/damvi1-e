package uf1_examen1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class NoRepeatPlease {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		
		ArrayList<Integer> llista = new ArrayList<Integer>();
		boolean flag = false;
		
		for (int i = 0; i < 6; i++) {
			llista.add(r.nextInt(49)+1);
		}
		
		System.out.println("La llista cont� aquests n�meros:");
		System.out.println(llista);
		
		for (int i = 0; i < llista.size(); i++) {
			int numero = llista.get(i);
			int buscador = Collections.frequency(llista, numero);
			
			if(buscador > 1) {
				flag = true;
			}
		}
		
		if(flag) {
			System.out.println("La lista contiene n�meros repetidos");
		}else {
			System.out.println("La lista no contiene n�meros repetidos");
		}
		
		System.out.println("Introduce un n�mero:");
		int numeroUsuario = sc.nextInt();
		
		if(llista.contains(numeroUsuario)) {
			System.out.println("SI est� en la lista");
		}else {
			System.out.println("NO est� en la lista");
		}
	}

}
