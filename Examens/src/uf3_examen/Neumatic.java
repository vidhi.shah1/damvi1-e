package uf3_examen;

import java.util.Random;

public class Neumatic extends Objecte {

	public boolean superNeumatic;
	Random r;
	
	public Neumatic() {
		r = new Random();
		int num = r.nextInt(2);
		
		if(num == 0) {
			this.superNeumatic = false;
		} else {
			this.superNeumatic = true;
		}
		
		this.TipusObjecte = TipusObjecte.DEFENSA;
	}

	@Override
	public String toString() {
		return "Neumatic -> superNeumatic=" + superNeumatic + ", r=" + r + ", prioritari=" + prioritari
				+ ", TipusObjecte=" + TipusObjecte;
	}
	
	
}
