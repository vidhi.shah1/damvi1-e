package uf3_examen;

public class Moneda extends Objecte{

	public int valor;
	
	public Moneda(int quinvalor) {
		this.valor = quinvalor;
		this.prioritari = true;
		this.TipusObjecte = TipusObjecte.PREMI;
	}

	public String toString() {
		return "Moneda -> valor=" + valor + ", prioritari=" + prioritari + ", TipusObjecte=" + TipusObjecte;
	}
}
