package uf3_examen;

public class MarioTeam extends Pilot{
	
	public MarioTeam(String nom) {
		super(nom);
		
	}

	@Override
	public void agafarObjecte(Objecte ob) {
		if(this.elMeuObjecte == null) {
			this.elMeuObjecte = ob;
			
		}else {
			if(this.elMeuObjecte.prioritari == false && ob.prioritari) {
				this.elMeuObjecte = ob;
				
			}else if(this.elMeuObjecte instanceof Moneda && ob instanceof Moneda) {
				((Moneda)this.elMeuObjecte).valor = ((Moneda)ob).valor;
				
			}else if(this.elMeuObjecte instanceof Platan && ob instanceof Platan) {
				this.elMeuObjecte = ob;
			}
		}
	}

	@Override
	public String cridar() {
		return "IUUUUUJUUUUU";
	}

	@Override
	public String toString() {
		return "MarioTeam [nom=" + nom + ", posicio=" + posicio + ", elMeuObjecte=" + elMeuObjecte + "]";
	}

	
}
