package uf3_examen;

public abstract class Objecte {

	protected boolean prioritari;
	protected TipusObjecte TipusObjecte;
	
	public Objecte() {
		this.prioritari = false;
	}

	public boolean isPrioritari() {
		return prioritari;
	}

	public void setPrioritari(boolean prioritazi) {
		this.prioritari = prioritazi;
	}

	public TipusObjecte getTipusObjecte() {
		return TipusObjecte;
	}

	public void setTipusObjecte(TipusObjecte tipusObjecte) {
		TipusObjecte = tipusObjecte;
	}
	
	public void llen�ar() {
		System.out.println("M'han llen�at");
	}
}
