package uf1_examen2;

import java.util.ArrayList;
import java.util.Scanner;

public class SusUltimasPalabras {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		ArrayList<String> llista = new ArrayList<String>();
		String paraula = sc.nextLine().toLowerCase();
		
		while (!paraula.equals("fi")) {
			llista.add(paraula);
			paraula = sc.nextLine();
		}
		
		if(llista.size() <= 5) {
			System.out.println(llista);
			
		} else {
			
			for (int i = llista.size()-5; i < llista.size(); i++) {
				System.out.println(llista.get(i));
			}
		}
		
	}

}
