package uf1_examen2;

import java.util.Scanner;

public class EscacsGemelosFacun {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Insereix un numero enter de peons:");
		int peons = sc.nextInt();
		
		int[][] matriu = new int[8][8];
		
		System.out.println("Insereix primer la fila i despres la columna on vols colocar el peo:");
		System.out.println("Tingues en compte que, la primera fila i columna es 0");
		for (int i = 0; i < peons; i++) {
			int posicioFila = sc.nextInt();
			int posicioColumna = sc.nextInt();
			
			matriu[posicioFila][posicioColumna] = 1;
		}
		
		int bessons = 0;
		
		for (int c = 0; c < 8; c++) {
			int contador = 0;
			
			for (int f = 0; f < 8; f++) {
				if(matriu[f][c] == 1) {
					contador++;
				}
			}
			
			if (contador > 1) {
				bessons++;
			}
			
		}
		
		System.out.println("El n�mero de bessons es: "+bessons);
		
		for (int f = 0; f < 8; f++) {
			for (int c = 0; c < 8; c++) {
				System.out.print(matriu[f][c]+" ");
			}
			System.out.println();
		}
	}

}
