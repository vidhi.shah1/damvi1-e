package uf1_examen2;

import java.util.Random;
import java.util.Scanner;

public class ElTesorillo {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		
		System.out.println("Insereix dimensions del jard� (primer files i despres columnes):");
		int files = sc.nextInt();
		int columnes = sc.nextInt();
		
		String[][] matriu = new String[files][columnes];
		
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				matriu[f][c] = ".";
			}
		}
		
		int filaTresor = r.nextInt(files);
		int columnaTresor = r.nextInt(columnes);
		matriu[filaTresor][columnaTresor] = "T";
		
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				System.out.print(matriu[f][c]+" ");
			}
			System.out.println();
		}
		
		System.out.println("Tresor amagat!");
		System.out.println("Indica quants intens vols (num enter):");
		int intents = sc.nextInt();
		
		while (intents != 0) {
			System.out.println("Insereix coordenades: ");
			int filaCandidata = sc.nextInt();
			int columnaCandidata = sc.nextInt();
			
			if (filaTresor == filaCandidata && columnaTresor == columnaCandidata) {
				System.out.println("ENHORABONA!!!!!!!! Has trobat el tresor!!!!!");
				break;
				
			}else {
				matriu[filaCandidata][columnaCandidata] = "E";
				intents--;
			}
		}
		
		if (intents == 0) {
			System.out.println("MALA SORT, No has trobat el tresor!!!!");
		}
		
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				System.out.print(matriu[f][c]+" ");
			}
			System.out.println();
		}
	}

}
