package uf1_examen2;

import java.util.Scanner;

public class Matador {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Insereix la seq��ncia de passes: ");
		String jugadors = sc.nextLine().toLowerCase();
		
		char ultim = jugadors.charAt(jugadors.length()-1);
		boolean flag = false;
		
		for (int i = 0; i < jugadors.length()-1; i++) {
			if(jugadors.charAt(i) == ultim) {
				flag = true;
			}
		}
		
		if(!flag) {
			System.out.println("KEMPES");
			
		} else {
			System.out.println("GOL");
		}
	}

}
