package uf2_examen1;

import java.util.Scanner;

public class Exercici1 {

	static Scanner sc; // necessito fer el Scanner estatic per possibles problemes.
	static char BUIT = '.';
	static char DIBUIX = 'x';

	// no se usar variables estaticas, siempre he declarado la longitud asi

	public static void main(String[] args) {

		sc = new Scanner(System.in);
		boolean exit = false;
		char[][] matriu = null;
		int[] longitud = null;

		do {

			mostrarMenu();
			int opcio = sc.nextInt();

			switch (opcio) {
			case 1:
				longitud = demanarLongitud();
				matriu = iniciarTauler(matriu, longitud);
				break;

			case 2:
				visualitzarTauler(matriu, longitud);
				break;

			case 3:
				int[] fc = punt(matriu, longitud);
				dibuixar(matriu, fc);
				break;

			case 4:
				break;

			case 5:
				exit = true;
				System.out.println("A10");
			}

		} while (!exit);

	}

	private static boolean dibuixar(char[][] matriu, int[] fc) {

		
		boolean flag;
		if (matriu[fc[0]][fc[1]] == BUIT) {
			matriu[fc[0]][fc[1]] = DIBUIX;
			flag = true;

		} else {
			matriu[fc[0]][fc[1]] = BUIT;
			flag = false;
		}
		
		return flag;
	}

	private static int[] punt(char[][] matriu, int[] longitud) {

		System.out.println("Insereix una posicio (fila)");
		int f = sc.nextInt();
		System.out.println("Insereix una posicio (columna)");
		int c = sc.nextInt();

		if (f > longitud[0] || c > longitud[1]) {
			System.out.println("Si us plau, digues un numero valid");
			punt(matriu, longitud);
		}

		int[] fc = {f, c};
		return fc;
		
	}

	private static int[] demanarLongitud() {

		System.out.println("Insereix numero de files i columnes");
		int files = sc.nextInt();
		int columnes = sc.nextInt();
		int[] longitud = { files, columnes };

		return longitud;
	}

	private static void visualitzarTauler(char[][] matriu, int[] longitud) {

		for (int i = 0; i < longitud[0]; i++) {
			for (int j = 0; j < longitud[1]; j++) {
				System.out.print(matriu[i][j] + " ");
			}

			System.out.println();
		}
	}

	private static char[][] iniciarTauler(char[][] matriu, int[] longitud) {

		matriu = new char[longitud[0]][longitud[1]];

		for (int i = 0; i < longitud[0]; i++) {
			for (int j = 0; j < longitud[1]; j++) {
				matriu[i][j] = BUIT;
			}
		}

		return matriu;
	}

	private static void mostrarMenu() {

		System.out.println("Escull una opcio valida: ");
		System.out.println("1- Iniciar tauler");
		System.out.println("2- Visualitzar tauler");
		System.out.println("3- Punt");
		System.out.println("4- Puntazo");
		System.out.println("5- Sortir");
	}

}
