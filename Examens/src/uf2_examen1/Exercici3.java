package uf2_examen1;

import java.util.Scanner;

public class Exercici3 {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introdueix un numero:");
		int num = sc.nextInt();
		
		int crides = recur(num);
		System.out.println();
		System.out.println("El total de crides es: "+crides);
	}

	private static int recur(int num) {

		if(num == 1) {
			System.out.print(num+" ");
			return 1;
		}else {
			if(num % 2 == 0) {
				System.out.print(num+" ");
				return 1 + recur(num/2);
			} else {
				System.out.print(num+" ");
				return 1 + recur((3*num)+1);
			}
		}
	}

}
