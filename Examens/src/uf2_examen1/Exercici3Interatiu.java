package uf2_examen1;

import java.util.Scanner;

public class Exercici3Interatiu {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introdueix un numero:");
		int num = sc.nextInt();
		int crides = 1;
		
		while(num != 1) {
			if(num % 2 == 0) {
				System.out.print(num+" ");
				num /= 2;
			} else {
				System.out.print(num+" ");
				num = (num*3)+1;
			}
			
			crides++;
		}
		System.out.print(num+" ");
		System.out.println();
		System.out.println("El total de crides es: "+crides);
	}

}
