package uf2_examen1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Exercici2 {

	static Scanner sc;
	public static void main(String[] args) {
	
		sc = new Scanner(System.in);
		boolean exit = false;
		ArrayList<Integer> llista = new ArrayList<Integer>();
		
		do {
			
			mostrarMenu();
			int opcio = sc.nextInt();
			
			switch(opcio) {
			case 1:
				puntuacions(llista);
				break;
				
			case 2:
				mostrarPuntuacions(llista);
				break;
				
			case 3:
				float resultat = calcularPuntuacions(llista);
				System.out.println("El resultat es: "+resultat);
				break;
				
			case 4:
				exit = true;
				System.out.println("A10");
				break;
			}
			
		}while(!exit);
		
	}
	
	private static float calcularPuntuacions(ArrayList<Integer> llista) {
		
		Integer gran = Collections.min(llista);
		Integer petit = Collections.max(llista);
		
		llista.remove(gran);
		llista.remove(petit);
		
		int suma = 0;
		for (int i = 0; i < 3; i++) {
			suma += llista.get(i);
		}
		
		float resultat = suma / 3;
		return resultat;
	}

	private static void mostrarPuntuacions(ArrayList<Integer> llista) {

		System.out.println(llista);
		
	}
	
	private static void puntuacions(ArrayList<Integer> llista) {
		
		System.out.println("Insereix 5 puntuacions: ");
		for (int i = 0; i < 5; i++) {
			llista.add(sc.nextInt());
		}
		
	}
	
	private static void mostrarMenu() {
		
		System.out.println("Escull una opcio valida: ");
		System.out.println("1- Obtenir puntuacions");
		System.out.println("2- Visualitzar Puntuacions");
		System.out.println("3- Obtenir nota final");
		System.out.println("4- Sortir");
		
	}

}
