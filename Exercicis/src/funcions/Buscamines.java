package funcions;

import java.util.Random;
import java.util.Scanner;

public class Buscamines {

	static Scanner sc;
	static Random r;
	
	public static void main(String[] args) {
		
		sc = new Scanner(System.in);
		r = new Random();
		System.out.println("--------------");
		System.out.println("- BUSCAMINAS -");
		System.out.println("--------------");
		int[][] mines = null;
		int[][] camp = null;
		boolean exit = false;
		
		do {
			
			mostrarMenu();
			int opcio = sc.nextInt();
			sc.nextLine();
			
			switch (opcio) {
			case 1:
				mostrarAjuda();
				break;
				
			case 2:
				int[] opcions = opcions();
				mines = iniciarMines(opcions);
				camp = iniciarCamp(opcions);
				mostrarTauler(mines);
				break;
				
			case 3:
				boolean partida = true;
				
				while(partida) {
					mostrarTauler(mines);
					mostrarTauler(camp);
				}
				
				break;
				
			case 4:
				veureRanking();
				break;
				
			case 0:
				exit = areUSure();
				break;
				
			default:
				System.out.println("Si us plau escull una opci� v�lida.");
				break;
			}
			
		}while(!exit);
		
	}

	private static void mostrarTauler(int[][] matriu) {
		
		for (int f = 0; f < matriu.length; f++) {
			for (int c = 0; c < matriu[0].length; c++) {
				System.out.print(matriu[f][c]+ " ");
			}
			System.out.println();
		}
		
		
	}

	private static int[][] iniciarCamp(int aux[]) {
		
		int camp[][] = new int[aux[0]][aux[1]];
		
		for (int f = 0; f < aux[0]; f++) {
			for (int c = 0; c < aux[1]; c++) {
				camp[f][c] = 9;
			}
		}
		
		System.out.println("camp iniciat");
		return camp;
	}

	private static int[][] iniciarMines(int aux[]) {
		
		int mines[][] = new int[aux[0]][aux[1]];
		int a = aux[2];
		
		while(a > 0) {
			int fila = r.nextInt(aux[0]);
			int columna = r.nextInt(aux[1]);
			
			if(mines[fila][columna] != 1) {
				mines[fila][columna] = 1;
				a--;
			}
		}
		System.out.println("mines iniciat");
		return mines;
		
	}

	private static boolean areUSure() {
		
		System.out.println("Segur que vols sortir?");
		System.out.println("Insereix 0 per sortir, o una altre cosa per cancelar");
		int opcio = sc.nextInt();
		boolean exit = false;
		
		if(opcio == 0) {
			System.out.println("Adeu :(");
			exit = true;
			return exit;
			
		} else {
			return exit;
		}

	}

	private static int[] opcions() {
		
		System.out.println("Insereix el vostre nom: ");
		String nom = sc.nextLine();
		System.out.println("Insereix n�mero de files i columnes (en aquest ordre)");
		int files = sc.nextInt();
		int columnes = sc.nextInt();
		System.out.println("Insereix n�mero de mines");
		int mines = sc.nextInt();
		int aux[] = {files, columnes, mines};
		
		return aux;
		
	}

	private static void veureRanking() {
		
		
	}
	
	private static void mostrarAjuda() {
		
		
	}
	
	private static void mostrarMenu() {
		
		System.out.println("1- Mostrar Ajuda");
		System.out.println("2- Opcions");
		System.out.println("3- Jugar partida");
		System.out.println("4- Veure Ranking");
		System.out.println("0- Sortir");
		
	}

}
