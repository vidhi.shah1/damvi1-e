package mokepon;

public class Revive extends Item{

	public Revive(String name) {
		super(name);
		
	}

	@Override
	public void use(MokemonCapturat mok) {

		if(mok.fainted) {
			mok.fainted = false;
			mok.current_hp = 1;
			this.quantity--;
			
		} else {
			System.out.println("El Mokemon no esta debilitat!");
		}
	}

}
