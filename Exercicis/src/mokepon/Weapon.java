package mokepon;

public class Weapon extends Item implements Equipment{

	int extraAttack;
	
	public Weapon(String name) {
		super(name);
	}

	public int getExtraAttack() {
		return extraAttack;
	}

	public void setExtraAttack(int extraAttack) {
		this.extraAttack = extraAttack;
	}

	@Override
	public void Equip(Mokemon mok) {
		mok.equippedItem = this;
		mok.atk += this.extraAttack;
	}

	@Override
	public void Unequip(Mokemon mok) {
		mok.equippedItem = null;
		mok.atk -= this.extraAttack;
	}

	@Override
	public void use(MokemonCapturat mok) {
		this.Equip(mok);
	}
	
	
}
