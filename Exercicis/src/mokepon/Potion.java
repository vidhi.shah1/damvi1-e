package mokepon;

public class Potion extends Item{

	int hp_curada;
	
	public Potion(String name) {
		super(name);
		this.hp_curada = 20;
	}
	
	public Potion(String name, int hp) {
        super(name);
        this.hp_curada = hp;
    }

	@Override
	public void use(MokemonCapturat mok) {
		
		if(!mok.fainted) {
			mok.current_hp += this.hp_curada;
			this.quantity--;
			
			if(mok.current_hp > mok.max_hp) {
				mok.current_hp = mok.max_hp;
			}
		}else {
			System.out.println("No es pot curar un Mokepon debilitat!!");
		}
		
	}

	
}
