package mokepon;

public class Armor extends Item implements Equipment{
	
	int extraDef;
	
	public Armor(String name) {
		super(name);

	}

	@Override
	public void Equip(Mokemon mok) {
		mok.equippedItem = this;
		mok.def += this.extraDef;
	}

	public int getExtraDef() {
		return extraDef;
	}

	public void setExtraDef(int extraDef) {
		this.extraDef = extraDef;
	}

	@Override
	public void Unequip(Mokemon mok) {
		mok.equippedItem = null;
		mok.def -= this.extraDef;
	}

	@Override
	public void use(MokemonCapturat mok) {
		this.Equip(mok);
	}
	
}
