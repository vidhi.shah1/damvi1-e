package mokepon;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class FitxersSerialitzats {

	public static void main(String[] args) {
		
		ArrayList<Item> uwu = recuperaObjectes();
		System.out.println(uwu);
		Potion p = recuperaPocioConcreta(100);
		System.out.println(p);
		MokemonCapturat mok = new MokemonCapturat();
		afegirMokepon(mok);
		
	}
	
	public static void afegirMokepon(MokemonCapturat Mok) {
		boolean trobat = false;
		try {
			File f = new File("resources/mokepon.dat");
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);

			try {
				while (true) {
					MokemonCapturat mokC = (MokemonCapturat) ois.readObject();
					System.out.println(mokC);
					if (mokC.equals(Mok)) {
						trobat = true;
						System.out.println("Aquest Mokepon ja era al Fitxer.");
					}
				}

			} catch (EOFException e) {
				System.out.println("Final del fitxer oleole.");
				ois.close();
			}

		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("excepci� d'entrada/sortida");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("no s'ha trobat la classe demanada");
			e.printStackTrace();
		}

		if (!trobat) {
			try {
				File f = new File("resources/mokepon.dat");

				boolean intervencionALaValentina = f.exists();
				// funciona de forma similar a un fileWriter, amb append incl�s
				FileOutputStream fos = new FileOutputStream(f, intervencionALaValentina);
				AppendableObjectOutputStream oos = new AppendableObjectOutputStream(fos, intervencionALaValentina);
				oos.writeObject(Mok);
				oos.flush();
				oos.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void afegirObjecte(Item obj) {
		try {
			File f = new File("resources/text/objectes.dat");
            //funciona de forma similar a un fileWriter, amb append incl�s
            FileOutputStream fos = new FileOutputStream(f, true);
            AppendableObjectOutputStream oos = new AppendableObjectOutputStream(fos, false);
            
            oos.writeObject(obj);
            
            oos.flush();
            oos.close();
            
		} catch (FileNotFoundException e) {
            e.printStackTrace();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public static Item recuperaObjecte() {
		try {
			File f = new File("resources/text/objectes.dat");
	        FileInputStream fis = new FileInputStream(f);
	        ObjectInputStream ois = new ObjectInputStream(fis);
	        
	        Item i = (Item) ois.readObject();
	        
	        if(i instanceof Potion) {
	        	 Potion p = (Potion) i;
	        	 ois.close();
	        	 return p;
	        	 
	        }else if(i instanceof Revive) {
	        	Revive r = (Revive) i;
	        	ois.close();
	        	return r;
	        }
            
	        ois.close();
	        
		} catch (FileNotFoundException e) {
	        System.out.println("no existeix el fitxer");
	        e.printStackTrace();
	        
	    } catch (IOException e) {
	        System.out.println("excepci� d'entrada/sortida");
	        e.printStackTrace();
	        
	    } catch (ClassNotFoundException e) {
	        System.out.println("no s'ha trobat la classe demanada");
	        e.printStackTrace();
	    }
		
		
		return null;
	}
	
	public static ArrayList<Item> recuperaObjectes() {
		try {
			File f = new File("resources/text/objectes.dat");
	        FileInputStream fis = new FileInputStream(f);
	        ObjectInputStream ois = new ObjectInputStream(fis);
	        ArrayList<Item> lista = new ArrayList<Item>();
	        
	        try {
		        while(true) {
		        	 Item i = (Item) ois.readObject();
		        	 
		        	 if(i instanceof Potion) {
			        	 Potion p = (Potion) i;
			        	 lista.add(p);
			        	 
			        }else if(i instanceof Revive) {
			        	Revive r = (Revive) i;
			        	lista.add(r);
			        	
			        }
		        	 
		        }
		        
	        } catch (EOFException e) {
	        	System.out.println("He acabao");
	        	ois.close();
	        	return lista;
	        }       
            
		} catch (FileNotFoundException e) {
	        System.out.println("no existeix el fitxer");
	        e.printStackTrace();
	        
	    } catch (IOException e) {
	        System.out.println("excepci� d'entrada/sortida");
	        e.printStackTrace();
	        
	    } catch (ClassNotFoundException e) {
	        System.out.println("no s'ha trobat la classe demanada");
	        e.printStackTrace();
	        
	    }
		

		return null;

	}
	
	public static Potion recuperaPocioConcreta(int n) {
		try {
			File f = new File("resources/text/objectes.dat");
	        FileInputStream fis = new FileInputStream(f);
	        ObjectInputStream ois = new ObjectInputStream(fis);
	        
	        try {
	        	 while(true) {
	        		 Item i = (Item) ois.readObject();
	        		 
	        		 if(i instanceof Potion) {
	        			 Potion p = (Potion) i;
	        			 
	        			 if(p.hp_curada == n) {
	        				 ois.close();
	        				 return p;
	        			 }
	        		 }
	        	 }
	        } catch (EOFException e) {
	        	System.out.println("He acabao");
	        	ois.close();
	        	return null;
	        }  
	        
		} catch (FileNotFoundException e) {
	        System.out.println("no existeix el fitxer");
	        e.printStackTrace();
	        
	    } catch (IOException e) {
	        System.out.println("excepci� d'entrada/sortida");
	        e.printStackTrace();
	        
	    } catch (ClassNotFoundException e) {
	        System.out.println("no s'ha trobat la classe demanada");
	        e.printStackTrace();
	        
	    }
		
		return null;
	}
}
