package mokepon;

import java.io.Serializable;

public abstract class Item implements Serializable{
	
	private String name;
	int quantity;
	
	public Item(String name){
		this.name = name;
		this.quantity = 1;
	}
	
	public void obtenir(int numObjectes) {
		this.quantity += numObjectes;
	}
	
	public void give(MokemonCapturat mok) {
		mok.item = this;
	}
	
	public abstract void use(MokemonCapturat mok);
}
