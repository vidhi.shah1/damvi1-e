package mokepon;

public interface Equipment {

	public abstract void Equip(Mokemon mok);
	
	public abstract void Unequip(Mokemon mok);
	
	public default boolean CanEquip(MokemonCapturat mok) {
		if(mok.equippedItem == null && !mok.fainted) {
			return true;
		} else {
			return false;
		}
	}
	
	public default boolean WrongEquip(MokemonCapturat mok) {
		if(mok.item instanceof Equipment) {
			return true;
		} else {
			return false;
		}
	}
}
