package mokepon;

import java.util.Scanner;

public class Test {

	static Scanner sc;

	public static void main(String[] args) {

		sc = new Scanner(System.in);

		Mokemon mulmasaur = new Mokemon("Mulmasaur", 5);
		Mokemon marmander = new Mokemon("Marmander", 5);

		mulmasaur.type = Types.PLANT;
		marmander.type = Types.FIRE;

		Attacks LatigoCepa = new Attacks("LatigoCepa", 20, Types.PLANT, 10);
		mulmasaur.addAttack(LatigoCepa);
		Attacks HojaAfilada = new Attacks("HojaAfilada", 50, Types.PLANT, 10);
		mulmasaur.addAttack(HojaAfilada);

		Attacks Pu�oFuego = new Attacks("Pu�oFuego", 20, Types.FIRE, 10);
		marmander.addAttack(Pu�oFuego);
		Attacks LanzaLlamas = new Attacks("LanzaLlamas", 50, Types.FIRE, 10);
		marmander.addAttack(LanzaLlamas);
		
		MokemonCapturat test = new MokemonCapturat();
		
		boolean exit = false;

		do {

			mostrarMenu();
			int opcio = sc.nextInt();

			switch (opcio) {
			case 1:
				lluita(mulmasaur, marmander);
				break;
				
			case 2:
				marmander.healing();
				mulmasaur.healing();
				System.out.println("Curados!");
				break;
				
			case 3:
				marmander.sayname();
				mulmasaur.sayname();
				break;
			
			case 4:
				MokemonCapturat elMeuMikachu = new MokemonCapturat();
				System.out.println(elMeuMikachu.name);
				elMeuMikachu.sayname();
				mulmasaur.atacar(elMeuMikachu, 0);
				
				break;
				
			case 5:
				test.pet();
				System.out.println(test.hapiness);
				break;
		
			case 0:
				System.out.println("a10");
				exit = true;
				break;
			}
			
		} while (!exit);
	}

	
	public static MokemonCapturat capturar(Mokemon mok, String trainerName, String nickname) {
		
		if(!(mok instanceof MokemonCapturat)) {
			MokemonCapturat algo = new MokemonCapturat(mok, nickname, trainerName);
			return algo;
			
		} else {
			System.out.println("No pots capturar un Mokemon que ja esta capturat");
			return (MokemonCapturat) mok;
		}
	}
	
	private static void lluita(Mokemon mulmasaur, Mokemon marmander) {

		System.out.println("Elige pokemon:");
		System.out.println("1- Marmander");
		System.out.println("2- Mulmasaur");

		int num = sc.nextInt();

		if (marmander.fainted || mulmasaur.fainted) {
			System.out.println("Pokemon debilitat!");
			
		} else {
			if (num == 1) {
				System.out.println("Elige ataque: ");
				System.out.println("1- Pu�o Fuego");
				System.out.println("2- Lanza Llamas");

				int ataque = sc.nextInt() - 1;
				marmander.atacar(mulmasaur, ataque);

			} else {

				System.out.println("Elige ataque: ");
				System.out.println("1- L�tigo Cepa");
				System.out.println("2- Hoja Afilada");

				int ataque = sc.nextInt() - 1;

				mulmasaur.atacar(marmander, ataque);
			}
		}

	}

	private static void mostrarMenu() {
		System.out.println("---------------------------");
		System.out.println("1- Lluita");
		System.out.println("2- Centro Mokemon");
		System.out.println("3- Estatisticas de Mokemons");
		System.out.println("4- Mokemon Capturat");
		System.out.println("5- Acariciar");
		System.out.println("0- Sortir");
		System.out.println("---------------------------");

	}

}
