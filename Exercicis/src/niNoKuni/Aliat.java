package niNoKuni;

public abstract class Aliat extends Personatge {

	protected String nom;

	public Aliat(String nom, int vida, int atac) {
		super(vida, atac);
		this.nom = nom;

	}

	@Override
	public String visualitzar() {
		return "Aliat [nom=" + nom + ", vidaActual=" + vidaActual + ", atac=" + atac + ", getClass()=" + getClass()
				+ "]";
	}

	public void utilizar(Objecte a) {

		if(!this.mort ) {
			if (this instanceof Marcs) {

				if (a instanceof Pergami) {
					this.vidaActual += a.getVit();
					if (this.vidaActual > this.vidaMax) {
						this.vidaActual = this.vidaMax;
					}

				} else {
					System.out.println("Els personatges de clase marc nomes poden utilizar pergamins magics!");
				}

			} else if (this instanceof Gatets) {

				if (a instanceof Pocio) {
					this.vidaActual += a.getVit();
					if (this.vidaActual > this.vidaMax) {
						this.vidaActual = this.vidaMax;
					}

				} else {
					System.out.println("Els personatges de clase gatets nomes poden utilizar pocions!");
				}
			}
			
		}else {
			System.out.println("Els personatges morts no poden utilizar objectes");
		}
	}

}
