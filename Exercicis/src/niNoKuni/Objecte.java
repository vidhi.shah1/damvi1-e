package niNoKuni;

public abstract class Objecte {

	private int vit;

	public Objecte(int vit) {
		this.vit = vit;
	}

	public int getVit() {
		return vit;
	}

	public void setVit(int vit) {
		this.vit = vit;
	}

	public String toString() {
		return "Objecte [vit=" + vit + "]";
	}
	
}
