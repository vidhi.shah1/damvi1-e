package niNoKuni;

public class Gatets extends Aliat implements ArmaDelMal {

	Peixos peix;

	public Gatets(String nom, int vida, int atac, Peixos peix) {
		super(nom, vida, atac);
		this.peix = peix;
	}

	@Override
	public void atacar(Personatge a) throws Exception {

		if (this.mort) {
			throw new Exception("EnElLobbyException");
		} else {
			if (a instanceof Enemic) {
				a.vidaActual -= this.atac;

				if (a.vidaActual <= 0) {
					a.morirse();
				}

				System.out.println("miau");

			} else {
				System.out.println("Estas intentant atacar a un aliat!");
			}
		}

	}

	public void riureMaligne() {
		
	}

}
