package matrius;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PanBimboTest {

	@Test
	void test() {
		char[][] aux =
            {
                    {' ', ' ', ' ', ' '},
                    {' ', 'X', 'X', ' '},
                    {' ', 'X', 'X', ' '},
                    {' ', ' ', ' ', ' '}
            };
			assertArrayEquals(aux, PanBimbo.iniciar(4));
			
		char[][] aux2 =
	        {
	                {' ', ' ', ' ', ' ', ' '},
	                {' ', 'X', 'X', 'X', ' '},
	                {' ', 'X', 'X', 'X', ' '},
	                {' ', 'X', 'X', 'X', ' '},
	                {' ', ' ', ' ', ' ', ' '}
	        };
				assertArrayEquals(aux2, PanBimbo.iniciar(5));
		char[][] aux3 =
            {
                    {' ', ' ', ' ', ' ', ' ', ' '},
                    {' ', 'X', 'X', 'X', 'X', ' '},
                    {' ', 'X', 'X', 'X', 'X', ' '},
                    {' ', 'X', 'X', 'X', 'X', ' '},
                    {' ', 'X', 'X', 'X', 'X', ' '},
                    {' ', ' ', ' ', ' ', ' ', ' '}
            };
			assertArrayEquals(aux3, PanBimbo.iniciar(6));
	}

}
