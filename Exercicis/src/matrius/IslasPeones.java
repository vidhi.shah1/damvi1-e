package matrius;

import java.util.Random;
import java.util.Scanner;

public class IslasPeones {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		
		boolean exit = false;
		boolean flag = false;
		int[][] matriu = new int[8][8];
		System.out.println("Exercici illes de Peons:");
		System.out.println();
		
		do {
			
			System.out.println("Escull n�mero");
			System.out.println("1- N�mero de peons que vols insertar");
			System.out.println("2- Llistat de la posici� de tots els peons");
			System.out.println("3- Total de illes");
			System.out.println("4- Mostrar taulell");
			System.out.println("5- Hi ha illes amb nom�s 1 pe�? (pe� aillat) *No funciona*"); //todo
			System.out.println("6- Reset");
			System.out.println("7- Sortir");
			
			int teclat = sc.nextInt();
			
			switch(teclat) {
			case 1:
				System.out.println("Insereix un n�mero enter:");
				int peons = sc.nextInt();
				
				while (peons > 0) {
					int filaR = r.nextInt(8);
					int columnaR = r.nextInt(8);
					
					if (matriu[filaR][columnaR] != 1) {
						matriu[filaR][columnaR] = 1;
						peons--;
					}
				}
				
				System.out.println("Gr�cies :)");
				System.out.println();
				break;
				
			case 2:
				System.out.println();
				for (int f = 0; f < 8; f++) {
					for (int c = 0; c < 8; c++) {
						if(matriu[f][c] == 1) {
							System.out.println(f+" "+c);
						}
					}
				}
				System.out.println();
				break;
				
			case 3:
				System.out.println();
				
				flag = false;
				int algo = 0;
				int islas = 0;
				
				for (int c = 0; c < 8; c++) {
					for (int f = 0; f < 8; f++) {
						if(matriu[f][c] == 1) {
							flag = true;
						}
					}
					
					if(flag) {
						algo++;
					}
					
					if(!flag && algo != 0) {
						islas++;
						algo = 0;
					}
					
					if(flag && c == 7) {
						islas++;
					}
					
					flag = false;
				}
				
				System.out.println("Hi ha un total de: "+islas+" illes :)");
				break;
				
			case 4:
				for (int f = 0; f < 8; f++) {
					for (int c = 0; c < 8; c++) {
						System.out.print(matriu[f][c]+" ");
					}
					System.out.println();
				}
				
				System.out.println();
				break;
				
			case 5:
				System.out.println();
				flag = false;
				int contador = 0;
				
				for (int c = 0; c < 8; c++) {
					for (int f = 0; f < 8; f++) {
						if(matriu[f][c] == 1) {
							contador++;
							flag = true;
						}
					}
					
					if(contador == 1 && !flag) {
						System.out.println("Hi ha peons aillats");
						contador = 0;
					}else {
						System.out.println("No hi ha peons aillats");
						contador = 0;
					}
					
				}
				break;
				
			case 6:
				for (int f = 0; f < 8; f++) {
					for (int c = 0; c < 8; c++) {
						matriu[f][c] = 0;
					}
					
				}
				System.out.println("El taullel est� resetejat :)");
				System.out.println();
				break;
				
			case 7:
				System.out.println("A10 :(");
				exit = true;
				break;
			}
			
		}while(!exit);
	}

}
