package recursivitat;

import java.util.Scanner;

public class Exercici1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Insereix numero per calcular el seu factorial");
		
		int num = sc.nextInt();
		
		int resultat = Factorial(num);
		
		System.out.println(resultat);
	}

	private static int Factorial(int num) {
		
		if (num == 1) {
			return 1;
			
		} else {
			
			return num * Factorial(num-1);
		}
		
	}

}
