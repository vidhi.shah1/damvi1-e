package part4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.Window;
import part1.Roca;

public class Joc5 {

	// declaracio field
	static Field f = new Field();
	// declaracio Window
	static Window w = new Window(f, 1500, 900);
	
	public static void main(String[] args) throws InterruptedException {
		f.background = "resources/suelito.png";
		Projectil projectil = new Projectil("bala", 0, 0, 0, 0, 0, "resources/espadita.png", f);
		Personatge pj1 = new Personatge("Link", 725, 425, 775, 475, 0, "resources/Link1.gif", f, projectil);
		timer.schedule(task1, 0, 1000);
		Roca rocaAr = new Roca("Parede", -1, -1, 1500, 0, 0, "resources/rock1.png", f, 0);
		Roca rocaD = new Roca("Parede", 1470, 0, 1501, 900, 0, "resources/rock1.png", f, 0);
		Roca rocaAb = new Roca("Parede", 0, 862, 1500, 901, 0, "resources/rock1.png", f, 0);
		Roca rocaI = new Roca("Pared", -1, 0, 0, 901, 0, "resources/rock1.png", f, 0);
		Puntos puntos = new Puntos("Puntos", 3, 3, 23, 23, 0, "", f, true);
		
		boolean sortir = false;

		while (!sortir) {
			f.draw();
			Thread.sleep(30);
			Set<Character> keys = input();
			pj1.movimentH(keys);
			pj1.movimentV(keys);
			Set<Character> keysd = inputd();
			puntos.punts = projectil.uwuwu;
			puntos.update();
			if (keysd.contains(' ')) {
				pj1.disparar();
			}
			
			if(Enemic.contador == 5) {
				System.out.println("Perdiste la partida :(");
				String nombre = w.showInputPopup("Nombre del jugador: ");
				String puntuacion = String.valueOf(projectil.uwuwu);
				w.close();
				afegirJugador(nombre, puntuacion);
				w.showPopup("La teva puntuacio es: "+puntuacion);
				String rank = mostrarRanking();
				w.showPopup(rank);
				sortir = true;
				
			}

		}
		
	}

	private static String mostrarRanking() {
		
		try {
			File f = new File("resources/ranking.txt");
			FileReader fr;
			fr = new FileReader(f);
			BufferedReader br =  new BufferedReader(fr);
			
			String salida = "";
			int i = 0;
			
			while(br.ready() && i < 5) {
				String[] aux = br.readLine().split(";");
				salida += aux[0]+" "+aux[1]+"\n";
				i++;

			}
			
			return salida;
			
		}catch (FileNotFoundException e) {
            System.out.println("El fitxer no existeix");
            e.printStackTrace();
            
        } catch (IOException e) {
            System.out.println("Excepci� general de lectura");
            e.printStackTrace();
        }
		
		return null;
	}

	private static void afegirJugador(String nombre, String puntuacion) {
		try {
			File f = new File("resources/ranking.txt");
			FileReader fr;
			fr = new FileReader(f);
			BufferedReader br =  new BufferedReader(fr);
			
            File f2 = new File("resources/ranking2.txt");
            FileWriter fw;
            fw = new FileWriter(f2, true);
            BufferedWriter bw = new BufferedWriter(fw);
            
            int puntuacionINT = Integer.parseInt(puntuacion);
            boolean flag = false;
            
            while(br.ready()) {
            	String[] aux = br.readLine().split(";");
            	int auxINT = Integer.parseInt(aux[1]);
            	
            	if(puntuacionINT >= auxINT && !flag) {
                    bw.append(nombre+";");
                    bw.append(puntuacion+"\n");
                    
                    bw.append(aux[0]+";");
            		bw.append(aux[1]+"\n");
            		flag = true;
            		
            	}else {
            		bw.append(aux[0]+";");
            		bw.append(aux[1]+"\n");
            	}
            }
            
            System.out.println("tengo que entrar");
            
            if(!flag) {
            	System.out.println("he entrado mi pana");
            	bw.append(nombre+";");
                bw.append(puntuacion+"\n");
            }
            
            br.close();
            bw.flush();
            bw.close();
            
            f.delete();
            f2.renameTo(f);
            
        } catch (FileNotFoundException e) {
            System.out.println("El fitxer no existeix");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Excepci� general d'escriptura");
            e.printStackTrace();
        }

		
	}

	private static Set<Character> input() {
		return w.getPressedKeys();
	}

	private static Set<Character> inputd() {
		return w.getKeysDown();
	}
	
	//Timer representa el temporitzador. Nom�s en cal un
    static Timer timer = new Timer();
    //tasca. Cada tasca es individual. si vols 5 events necessitaras 5 tasques.
    static TimerTask task1 = new TimerTask() {
        //la funci� run indica tot all� que volguem executar
        @Override
        public void run()
        {
            //en aquest cas volem executar la funci� de generarEnemic
                Spawner.generarEnemic();
        }
    };

}
