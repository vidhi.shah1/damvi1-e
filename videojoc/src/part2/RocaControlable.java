package part2;

import Core.Field;
import part1.Roca;

public class RocaControlable extends Roca{

	public RocaControlable(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int accions) {
		
		super(name, x1, y1, x2, y2, angle, path, f, accions);

	}

	public void moviment(Input in) {
		
		if(in == Input.AMUNT) {
			this.y1-=2;
			this.y2-=2;
			this.accionsDisponibles--;
		}
		
		if(in == Input.AVALL) {
			this.y1+=2;
			this.y2+=2;
			this.accionsDisponibles--;
		}
		
		if(in == Input.DRETA) {
			this.x1+=2;
			this.x2+=2;
			this.accionsDisponibles--;
		}
		
		if(in == Input.ESQUERRA) {
			this.x1-=2;
			this.x2-=2;
			this.accionsDisponibles--;
		}
		
		
	}

}
