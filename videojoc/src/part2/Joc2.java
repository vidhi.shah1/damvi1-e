package part2;

import Core.Field;
import Core.Window;
import part1.Roca;

public class Joc2 {

	// declaracio field
	static Field f = new Field();
	// declaracio Window
	static Window w = new Window(f);
	
	public static void main(String[] args) throws InterruptedException {
		RocaControlable roca = new RocaControlable("Roca", 50, 50, 180, 150, 0, "resources/rock1.png", f, 50);
		boolean sortir = false;

		while (!sortir) {
			f.draw();
			Thread.sleep(30);
			input(roca);
			
			/*
			 * 	if(roca.accionsDisponibles == 0) {
					roca.delete();
				}
			 */

		}
	}

	private static void input(RocaControlable rocaControlable) {
		
		if(w.getPressedKeys().contains('d')) {
			rocaControlable.moviment(Input.DRETA);
		}
		

		if(w.getPressedKeys().contains('a')) {
			rocaControlable.moviment(Input.ESQUERRA);
		}
		

		if(w.getPressedKeys().contains('w')) {
			rocaControlable.moviment(Input.AMUNT);
		}
		

		if(w.getPressedKeys().contains('s')) {
			rocaControlable.moviment(Input.AVALL);
		}
		
	}

}
