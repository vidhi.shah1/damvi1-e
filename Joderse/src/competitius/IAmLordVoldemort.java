package competitius;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/*
 * https://joder.ga/problem/lordvoldemort
 */

public class IAmLordVoldemort {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		sc.nextLine();
		
		for (int i = 0; i < casos; i++) {
			String frase = sc.nextLine();
			frase = frase.replace(" ", "");
			ArrayList<String> llista = new ArrayList<String>();
			
			for (int j = 0; j < frase.length(); j++) {
				String letra = Character.toString(frase.charAt(j));
				llista.add(letra);
			}
			
			Collections.sort(llista);
			
			frase = sc.nextLine();
			frase = frase.replace(" ", "");
			ArrayList<String> llista2 = new ArrayList<String>();
			
			for (int j = 0; j < frase.length(); j++) {
				String letra = Character.toString(frase.charAt(j));
				llista2.add(letra);
			}
			
			Collections.sort(llista2);
			
			if(llista.equals(llista2)) {
				System.out.println("SI");
			}else {
				System.out.println("NO");
			}
		}
	}

}
