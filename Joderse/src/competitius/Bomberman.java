package competitius;

import java.util.Scanner;

/*
 * https://joder.ga/problem/bomberman
 */

public class Bomberman {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int files = sc.nextInt();
		int columnes = sc.nextInt();
		int [][] matriu = new int[files][columnes];
		
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				matriu[f][c] = sc.nextInt();
			}
		}
		
		int posicioF = sc.nextInt();
		int posicioC = sc.nextInt();
		int punts = 0;
		
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				if (f == posicioF || c == posicioC) {
					punts += matriu[f][c];
				}
			}
		}
		
		System.out.println(punts);
	}

}
