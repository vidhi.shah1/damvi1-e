package competitius;

import java.util.Scanner;

/*
 * https://joder.ga/problem/basketmania
 */

public class BasketMania {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int partidos = sc.nextInt();
		
		for (int i = 0; i < partidos; i++) {
			int cosas = sc.nextInt();
			sc.nextLine();
			int local = 0;
			int visitant = 0;
			int punts = 0;
			
			for (int j = 0; j < cosas; j++) {
				String puntuacio = sc.next();
				
				switch (puntuacio) {
				case "V":
					punts = sc.nextInt();
					visitant += punts;
					break;
					
				case "L":
					punts = sc.nextInt();
					local += punts;
					break;
				}
				
			}
			
			if (local == visitant) {
				System.out.print("E ");
				
			}else if (local > visitant){
				System.out.print("L ");
				
			}else if (visitant > local) {
				System.out.print("V ");
			}
			
			System.out.print(local+" "+visitant);
			System.out.println();
		}
	}

}
