package competitius;

import java.util.ArrayList;
import java.util.Scanner;

/*
 * https://joder.ga/problem/premiinesperat
 */

public class PremiInesperat {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int numerito = sc.nextInt();

		while (numerito != 0) {
			ArrayList<Integer> llista = new ArrayList<>();

			while (numerito != 0) {
				llista.add(numerito);
				numerito = sc.nextInt();
			}

			int carlota = llista.get(sc.nextInt() - 1);
			int joana = llista.get(sc.nextInt() - 1);

			if (carlota < joana) {
				System.out.println("CARLOTA");

			} else {
				System.out.println("JOANA");
			}

			llista.clear();
			carlota = 0;
			joana = 0;
			numerito = sc.nextInt();
		}
	}

}
