package competitius;

import java.util.Scanner;

/*
 * https://joder.ga/problem/piramidecartes
 */

public class PiramideDeCartes {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int cartes = sc.nextInt();
			int quan = 2;
			int pis = 0;
			
			while (cartes >= quan) {
				pis++;
				cartes -= quan;
				quan += 3;
			}
			
			System.out.println(pis+" "+cartes);
		}
	}

}
