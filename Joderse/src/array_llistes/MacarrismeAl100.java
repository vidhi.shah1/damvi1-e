package array_llistes;

import java.util.Scanner;

/*
 * https://joder.ga/problem/stringclub1
 */

public class MacarrismeAl100 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			double numerito = sc.nextDouble();
			double aux =  numerito * 100;
			
			System.out.print(aux+"% ");
			
		}
		
		System.out.println();
	}

}
