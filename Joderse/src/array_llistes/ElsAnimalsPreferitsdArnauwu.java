package array_llistes;

import java.util.Scanner;

/*
 * https://joder.ga/problem/animalsarnauwu
 */

public class ElsAnimalsPreferitsdArnauwu {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		int longitud = 0;
		boolean igual = false;
		
		for (int i = 0; i < casos; i++) {
			longitud = sc.nextInt();
			sc.nextLine();
			String[] array = new String[longitud];
			
			for (int j = 0; j < array.length; j++) {
				array[j] = sc.nextLine();
			}
			
			for (int j = 0; j < array.length-1; j++) {
				if(array[j].equals(array[longitud-1])) {
					igual = true;
				}
			}
			
			if(igual) {
				System.out.println("SI");
				igual = false;
				
			}else{
				System.out.println("NO");
				igual = false;
			}
			
		}
		
	}

}