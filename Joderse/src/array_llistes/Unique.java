package array_llistes;

import java.util.HashSet;
import java.util.Scanner;

/*
 * https://joder.ga/problem/unique
 */

public class Unique {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		HashSet<String> set = new HashSet<>();
		
		for (int i = 0; i < casos; i++) {
			int longitud = sc.nextInt();
			sc.nextLine();
			
			for (int j = 0; j < longitud; j++) {
				set.add(sc.nextLine());
			}
			
			System.out.println(set);
			set.clear();
		}
	}

}
