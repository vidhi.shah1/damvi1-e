package array_llistes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/*
 * https://joder.ga/problem/freq
 */

public class Freq��ncia {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int numeritos = sc.nextInt();
			ArrayList<Integer> llista = new ArrayList<>();
			
			for (int j = 0; j < numeritos; j++) {
				llista.add(sc.nextInt());
			}

			for (int j = 0; j < 10; j++) {
				System.out.print(Collections.frequency(llista, j)+" ");
			}
			
			System.out.println();
		}
	}

}
