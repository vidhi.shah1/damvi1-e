package array_llistes;

import java.util.Scanner;

/*
 * https://joder.ga/problem/introarrays2
 */

public class EscriuUnArray2 {

	public static void main(String[] args) {
		
		Scanner michi = new Scanner(System.in);
		
		int k = michi.nextInt();
		String aux = michi.nextLine();
		
		String[] a = new String[k];
		
		for (int j = 0; j < a.length; j++) {
			a[j] = michi.nextLine();
		}

		int n = michi.nextInt();
		
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
		
		System.out.println(a[n]);
	}

}