package array_llistes;

import java.util.Scanner;
import java.util.TreeSet;

/*
 * https://joder.ga/problem/guarderiapokemon
 */

public class GuarderiaPokemon {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String tecla = sc.nextLine();
		TreeSet<String> llista = new TreeSet<String>();
		
		while (!tecla.equals("D")) {
			switch (tecla) {
			
			case "A":
				llista.add(sc.nextLine());
				break;
				
			case "B":
				llista.remove(sc.nextLine());
				break;
				
			case "C":
				llista.clear();
				break;
			}
			
			tecla = sc.nextLine();
		}
		
		System.out.println(llista);
	}

}
