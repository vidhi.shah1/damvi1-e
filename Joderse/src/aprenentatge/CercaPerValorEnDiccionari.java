package aprenentatge;

import java.util.HashMap;
import java.util.Scanner;

public class CercaPerValorEnDiccionari {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			HashMap<String, String> diccionari = new HashMap<String, String>();
			int longitud = sc.nextInt();
			sc.nextLine();
			
			for (int j = 0; j < longitud-1; j++) {
				String aux = sc.nextLine();
				String[] split = aux.split("-");
				diccionari.put(split[0], split[1]);
			}
			
			String capital = sc.nextLine();
			String pais = "";
			
			for (String string : diccionari.keySet()) {
				if (diccionari.get(string).equals(capital)) {
					pais = string;
				}
			}
			
			System.out.println(diccionari);
			System.out.println(pais);
		}
	}

}
