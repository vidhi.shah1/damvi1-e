package aprenentatge;

import java.util.Scanner;

public class Rectangle {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int files = sc.nextInt();
		int columnes = sc.nextInt();
		int filaInicial = sc.nextInt();
		int columnaInicial = sc.nextInt();
		int filaFinal = sc.nextInt();
		int columnaFinal = sc.nextInt();
		int[][] matriu = new int[files][columnes];
		
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				
				if (c >= columnaInicial && f >= filaInicial && c <= columnaFinal && f <= filaFinal) {
					System.out.print("X ");
					
				}else {
					System.out.print(". ");
				}

			}
			System.out.println();
		}
	}

}
