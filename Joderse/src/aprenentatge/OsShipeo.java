package aprenentatge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class OsShipeo {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int longitudLlista = sc.nextInt();
			ArrayList<Integer> llista = new ArrayList<>();
			
			for (int j = 0; j < longitudLlista; j++) {
				llista.add(sc.nextInt());
				
			}
			
			int min = llista.get(0);
			int max = Collections.max(llista);
			int aux = max - min;
			
			System.out.println(aux);
		}
	}

}
