package aprenentatge;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class ElPrimerQueArribiA5 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		LinkedHashMap<String, Integer> guanyadors = new LinkedHashMap<String, Integer>();
		
		String nom = sc.nextLine();
		
		while (!nom.equals("xxx")) {
			if (guanyadors.containsKey(nom)) {
				int valor = guanyadors.get(nom);
				valor++;
				guanyadors.put(nom, valor);
				
			}else {
				guanyadors.put(nom, 1);
			}
			
			nom = sc.nextLine();
			
		}
		
		boolean flag = false;
		String nombre = "";
		
		for (String guanyador : guanyadors.keySet()) {
			if(guanyadors.get(guanyador) >= 5) {
				flag = true;
				nombre = guanyador;
				break;
			}
		}
		
		if(flag) {
			System.out.println(nombre);
		} else {
			System.out.println("NO");
		}
		
	}

}
