package aprenentatge;

import java.util.Scanner;

public class ParaulaMesLlarga {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		sc.nextLine();
		
		for (int i = 0; i < casos; i++) {
			String frase = sc.nextLine();
			String[] palabritas = frase.split(" ");
			int aux = 0;
			
			for (int j = 0; j < palabritas.length; j++) {
				String palabrita = palabritas[j];
				int longitudPalabrita = palabrita.length();
				
				if (longitudPalabrita > aux) {
					aux = longitudPalabrita;
				}
			}
			
			System.out.println(aux);
		}
		

	}

}
