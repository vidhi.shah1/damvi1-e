package aprenentatge;

import java.util.Scanner;

public class ComptarEnArrays {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		int num = 0;
		int contador = 0;
		int repetido = 0;
		
		for (int i = 0; i < casos; i++) {
			num = sc.nextInt();
			int[] array = new int[num];
			
			for (int j = 0; j < array.length; j++) {
				array[j] = sc.nextInt();
			}
			
			repetido = sc.nextInt();
			for (int j = 0; j < array.length; j++) {
				if (array[j] == repetido) {
					contador++;
				}
			}
			System.out.println(contador);
			contador = 0;
			repetido = 0;
		}
	}

}
