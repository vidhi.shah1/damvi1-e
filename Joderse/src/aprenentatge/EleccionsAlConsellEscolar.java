package aprenentatge;

import java.util.Scanner;

public class EleccionsAlConsellEscolar {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		
		String[][] matriu = new String[num][num];
		
		for (int f = 0; f < num; f++) {
			int aux = num - 1;
			for (int c = 0; c < num; c++) {
				if (f == 0 || c == 0 || f == num-1 || c == num-1) {
					System.out.print("X");
					
				}else if (f == c || f == aux){
					System.out.print("X");
					
				} else {
					System.out.print(".");
				}
				aux--;
			}
			
			System.out.println();
			
		}
	}

}
