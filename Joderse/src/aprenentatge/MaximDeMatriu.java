package aprenentatge;

import java.util.Scanner;

public class MaximDeMatriu {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int files = sc.nextInt();
			int columnes = sc.nextInt();
			int[][] matriu = new int[files][columnes];
			int auxf = 0;
			int auxc = 0;
			
			for (int f = 0; f < files; f++) {
				for (int c = 0; c < columnes; c++) {
					matriu[f][c] = sc.nextInt();
					
				}
			}
			
			int aux = 0;
			
			for (int f = 0; f < files; f++) {
				for (int c = 0; c < columnes; c++) {
					int num = matriu[f][c];
					
					if (num > aux) {
						aux = num;
						auxf = f+1;
						auxc = c+1;
					}
				}
			}
			
			System.out.println(auxf+" "+auxc);
		}
	}

}
