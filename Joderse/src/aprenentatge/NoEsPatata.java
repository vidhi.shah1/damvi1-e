package aprenentatge;

import java.util.Scanner;

public class NoEsPatata {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		sc.nextLine();
		
		for (int i = 0; i < casos; i++) {
			String frase = sc.nextLine().toLowerCase();
			
			if (frase.equals("patata")) {
				System.out.println("Es Patata");
			}else {
				System.out.println("No es Patata");
			}
		}
	}

}
