package aprenentatge;

import java.util.Scanner;

public class CambialoUnPoco {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();
		int longitud = 0;
		int numComparar = 0;
		int numCambiar = 0;
		
		for (int i = 0; i < casos; i++) {
			longitud = sc.nextInt();
			int[] array = new int[longitud];

			for (int j = 0; j < array.length; j++) {
				array[j] = sc.nextInt();
			}
			
			numComparar = sc.nextInt();
			numCambiar = sc.nextInt();
			
			for (int k = 0; k < array.length; k++) {
				
				if (array[k] == numComparar) {
					array[k] = numCambiar;
				}
			}
			
			for (int l = 0; l < array.length; l++) {
				System.out.print(array[l]+" ");
			}
			
			System.out.println();
			numComparar = 0;
			numCambiar = 0;
		}

	}

}
