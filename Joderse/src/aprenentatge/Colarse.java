package aprenentatge;

import java.util.ArrayList;
import java.util.Scanner;

public class Colarse {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int numeritos = sc.nextInt();
			ArrayList<Integer> lista = new ArrayList<>();
			
			for (int j = 0; j < numeritos; j++) {
				lista.add(sc.nextInt());
			}
			
			int c = sc.nextInt();
			int p = sc.nextInt();
			
			lista.add(p, c);
			
			for (int j = 0; j < lista.size(); j++) {
				System.out.print(lista.get(j)+" ");
			}
			
			System.out.println();
		}
	}

}
