package aprenentatge;

import java.util.ArrayList;
import java.util.Scanner;

public class Censura {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int num = sc.nextInt();
			sc.nextLine();
			ArrayList<String> llista = new ArrayList<String>();
			
			for (int j = 0; j < num-1; j++) {
				llista.add(sc.nextLine());
			}
			
			String frase = sc.nextLine();
			
			for (int j = 0; j < num-1; j++) {
				frase = frase.replace(llista.get(j), "*****");
			}
			
			System.out.println(frase);
		}

	}

}
