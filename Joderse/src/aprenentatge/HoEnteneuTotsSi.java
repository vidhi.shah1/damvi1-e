package aprenentatge;

import java.util.ArrayList;
import java.util.Scanner;

public class HoEnteneuTotsSi {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		sc.nextLine();
		
		for (int i = 0; i < casos; i++) {
			String frase = sc.nextLine();
			int posicion = sc.nextInt()-1;
			sc.nextLine();
			int longitudString = frase.length();
			ArrayList<String> llista = new ArrayList<String>();
			
			for (int j = 0; j < longitudString; j++) {
				char c = frase.charAt(j);
				String gerardGuapo = Character.toString(c);
				llista.add(gerardGuapo);
				
				if(j == posicion) {
					llista.add(", si?");
				}
			}
			
			if (posicion == longitudString) {
				llista.add(", si?");
			}
			
			for (int j = 0; j < llista.size(); j++) {
				System.out.print(llista.get(j));
			}
			
			System.out.println();
		}
		
	}

}
