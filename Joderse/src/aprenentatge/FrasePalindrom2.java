package aprenentatge;

import java.util.Scanner;

public class FrasePalindrom2 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();
		sc.nextLine();

		for (int i = 0; i < casos; i++) {
			boolean flag = false;
			String linea = sc.nextLine();
			String lineaLimpia = linea.replace(" ", "");
			lineaLimpia = lineaLimpia.toLowerCase();
			char[] caracteres = lineaLimpia.toCharArray();
			int longitud = caracteres.length-1;
			int mitad = longitud / 2;
			
			for (int j = 0; j < mitad; j++) {
				
				if(caracteres[j] != caracteres[longitud - j]) {
					flag = true;
				}
			}
			
			if (flag) {
				System.out.println("NO");
				
			}else {
				System.out.println("SI");
			}
		}

	}

}