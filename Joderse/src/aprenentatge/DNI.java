package aprenentatge;

import java.util.Scanner;

public class DNI {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		sc.nextLine();
		String[] letras = {"T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"};
		
		for (int i = 0; i < casos; i++) {
			StringBuilder DNI = new StringBuilder(sc.nextLine());
			char letra = DNI.charAt(8);
			String letrita = Character.toString(letra);
			DNI.deleteCharAt(8);
			String aux = DNI.toString();
			int numero = Integer.parseInt(aux);
			int aux2 = numero % 23;
			
			if (letrita.equals(letras[aux2])) {
				System.out.println("valid");
			}else {
				System.out.println("invalid");
			}
		}
	}

}