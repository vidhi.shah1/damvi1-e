package aprenentatge;

import java.util.Scanner;

public class MatriuIdentitat {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		
		int[][] matriu = new int[num][num];
		
		for (int f = 0; f < num; f++) {
			for (int c = 0; c < num; c++) {
				
				if (f == c) {
					System.out.print("1 ");
				} else {
					System.out.print("0 ");
				}
			}
			System.out.println();
		}
	}

}
