package recursivitat;

import java.util.Scanner;

/*
 * https://joder.ga/problem/bitlles
 */

public class Bitlles {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner (System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int pis = sc.nextInt();
			int total = Bitlles(pis);
			System.out.println(total);
		}
	}

	private static int Bitlles(int pis) {
		
		if (pis == 0) {
			return 0;
		} else if(pis == 1) {
			return 1;
		}else {
			return pis + Bitlles(pis-1);
		}
	}

}
