package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/positiusnegatius
 */

public class Bucles1SumaPositiusINegatius {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int num = sc.nextInt();
		int positiu = 0;
		int negatiu = 0;

		while (num != 0) {

			if (num > 0) {
				positiu++;

			} else {
				negatiu++;
			}

			num = sc.nextInt();

		}

		if (positiu > negatiu) {
			System.out.println("POSITIUS");

		} else if (negatiu > positiu) {
			System.out.println("NEGATIUS");

		} else {
			System.out.println("IGUALS");
		}
	}

}
