package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/mesgranpetit
 */

public class Bucles1ValorMesGraniMesPetit {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		int petit = 100000;
		int gran = -100000;
		
		while (num != 0) {
			
			if (num < petit) {
				petit = num;
			}
			
			if (num > gran) {
				gran = num;
			}
			
			num = sc.nextInt();
		}
		
		System.out.println(gran+" "+petit);
	}

}
