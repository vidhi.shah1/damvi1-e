package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/contarla
 */

public class ContarLA {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		sc.nextLine();
		
		for (int i = 0; i < casos; i++) {
			String frase = sc.nextLine().toLowerCase();
			int contador = 0;
			
			for (int j = 1; j < frase.length(); j++) {
				if(frase.charAt(j) == 'a' && frase.charAt(j-1) == 'l') {
					contador++;
				}
			}
			
			System.out.println(contador);
		}
	}

}
