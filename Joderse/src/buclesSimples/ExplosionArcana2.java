package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/explosionarcana2
 */

public class ExplosionArcana2 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int ini = sc.nextInt();
		int acuIni = ini;
		int hp = sc.nextInt();
		int q = 0;
		
		while (hp > 0) {
			hp = hp - acuIni;
			acuIni = acuIni + ini;
			q++;
		}
		
		System.out.println(q);
	}

}