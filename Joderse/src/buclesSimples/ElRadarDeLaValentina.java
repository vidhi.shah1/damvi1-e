package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/radarvalentina
 */

public class ElRadarDeLaValentina {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();

		for (int i = 0; i < casos; i++) {
			int M = 0;
			int B = 0;
			int H = 0;

			for (int j = 0; j < 5; j++) {
				int num = sc.nextInt();
				
				if (num >= 10000) {
					M++;
					
				} else if (num >= 1000) {
					B++;
					
				} else {
					H++;
					
				}

			}

			if (M >= 1) {
				System.out.println("M");

			} else if (B >= 1) {
				System.out.println("B");

			} else {
				System.out.println("H");
			}

		}

	}

}
