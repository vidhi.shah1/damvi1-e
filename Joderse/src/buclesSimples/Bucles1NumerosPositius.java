package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/numerospositius
 */

public class Bucles1NumerosPositius {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int num = sc.nextInt();
		int j = 0;

		for (int i = 0; i < num; i++) {

			int e = sc.nextInt();

			if (e > 0) {
				j++;
			}

		}

		System.out.println(j);
	}

}
