package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/paresinones
 */

public class ParesiNones {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		int parell = 0;
		int senar = 0;
		int num = 0;
		int aux = 0;
		
		for (int i = 0; i < casos; i++) {	
			num = sc.nextInt();
			String str = num+"";
			
			for (int j = 0; j < str.length(); j++) {
				char c = str.charAt(j);
				num = c-48;
				aux = j % 2;
				
				if (aux == 0) {
					parell+= num;
					
				}else {
					senar+= num;
				}
				
			}
			
			System.out.println(parell+" "+senar);
			aux = 0;
			parell = 0;
			senar = 0;
		}
		
	}

}