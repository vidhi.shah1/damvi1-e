package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/compteambelveri
 */

public class CompteAmbElVerí {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();
		int hp = 0;
		int rammus = 0;
		int twitch = 0;
		int ronda = 1;

		for (int i = 0; i < casos; i++) {
			ronda = 1;
			hp = sc.nextInt();
			rammus = sc.nextInt();
			twitch = sc.nextInt();

			while (hp > 0) {
				hp = hp - rammus;

				if (hp > 0) {
					hp = hp - twitch;

					if (hp <= 0) {
						System.out.println("TWITCH " + ronda);
					}

				} else {
					System.out.println("RAMMUS " + ronda);
				}

				ronda++;
			}

		}
	}

}