package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/controldenoms
 */

public class ControlDeNoms {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String nom = sc.nextLine();
		int longitud = nom.length();
		boolean flag = false;
		
		for (int i = 1; i < longitud; i++) {
			char letra1 = nom.charAt(i);
			char letra0 = nom.charAt(i-1);
			
			if (letra1 != letra0) {
				flag = true;
				break;
			}
		}
		
		if (flag) {
			System.out.println("SI");
			
		}else {
			System.out.println("NO");
		}
	}

}
