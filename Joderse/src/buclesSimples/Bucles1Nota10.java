package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/nota10
 */

public class Bucles1Nota10 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		int totalnotas = 0;
		int total10 = 0;
		
		
		while (num != -1) {
			
			if ( num >=0 && num <=10) {
				totalnotas++;
				
				if (num == 10) {
					total10++;
				}
			}
			
			num = sc.nextInt();
		}
		
		System.out.println("TOTAL NOTES: "+totalnotas+" NOTES10: "+total10);
	}

}
