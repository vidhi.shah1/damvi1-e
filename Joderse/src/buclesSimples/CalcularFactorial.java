package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/factorial
 */

public class CalcularFactorial {

	public static void main(String[] args) {
		
		Scanner gatitos = new Scanner(System.in);
		
		int casos = gatitos.nextInt();
		long num = 0;
		
		for (int i = 0; i < casos; i++) {
			num = gatitos.nextLong();
			long resultado = 1;
			
			for (int j = 1; j <= num; j++) {
				resultado = resultado * j;
				
			}
			System.out.println(resultado);
		}
	}

}