package matrius;

import java.util.Scanner;

/*
 * https://joder.ga/problem/blackfriday
 */

public class BlackFriday {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int filas = sc.nextInt();
		int columnas = sc.nextInt();
		
		int[][] matriu = new int[filas][columnas];
		
		for (int f = 0; f < filas; f++) {
			for (int c = 0; c < columnas; c++) {
				matriu[f][c] = sc.nextInt();
			}
		}
		
		int multiplicar = sc.nextInt();
		
		for (int f = 0; f < filas; f++) {
			for (int c = 0; c < columnas; c++) {
				matriu[f][c] *= multiplicar;
				System.out.print(matriu[f][c]+" ");
			}
			System.out.println();
		}
		
	}

}
