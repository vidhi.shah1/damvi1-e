package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/etsjove
 */

public class EtsJove {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int edad = sc.nextInt();
		
		if (edad < 30) {
			System.out.println("SI");
			
		}else {
			System.out.println("NO");
			
		}
	}

}
