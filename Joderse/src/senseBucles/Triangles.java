package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/triangles
 */

public class Triangles {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		double base = sc.nextDouble();
		double altura = sc.nextDouble();
		
		double r = (base * altura) /2;
		
		System.out.println(+r);
	}

}
