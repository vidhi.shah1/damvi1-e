package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/barrethogwarts
 */

public class ElBarretDeHogwarts {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		String respuesta = sc.nextLine();

		switch (respuesta) {
		case "Coratge":
			System.out.println("Gryffindor");
			break;

		case "Coneixement":
			System.out.println("Ravenclaw");
			break;

		case "Ambicio":
			System.out.println("Slytherin");
			break;

		default:
			System.out.println("Hufflepuff");
		}
	}

}
