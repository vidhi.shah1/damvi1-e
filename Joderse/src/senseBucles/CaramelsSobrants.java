package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/caramelssobrants
 */

public class CaramelsSobrants {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int caramels = sc.nextInt();
		int nebots = sc.nextInt();
		
		if (caramels == 0 || nebots == 0) {
			System.out.println("0");
			
		}else{
			int repartir = caramels % nebots;
			System.out.println(+repartir);
			
		}
	}

}
