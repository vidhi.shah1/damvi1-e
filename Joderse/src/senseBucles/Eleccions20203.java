package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/eleccions20203
 */

public class Eleccions20203 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int Jiden = sc.nextInt();
		int Drump = sc.nextInt();
		int Banders = sc.nextInt();
		
		if (Jiden > Drump && Jiden > Banders) {
			System.out.println("Jiden");
			
		} else if (Drump > Jiden && Drump > Banders) {
			System.out.println("Drump");
			
		}else if (Banders > Drump && Banders > Jiden){
			System.out.println("Banders");
			
		}
	}

}
