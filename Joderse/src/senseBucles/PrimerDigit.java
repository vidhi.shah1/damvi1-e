package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/primerdigit
 */

public class PrimerDigit {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String a = sc.nextLine();
		String b = sc.nextLine();
		
		char num1 = a.charAt(0);
		char num2 = b.charAt(0);
		
		if (num1 > num2) {
			System.out.println("A");
			
		}else {
			System.out.println("B");
		}
	}

}
