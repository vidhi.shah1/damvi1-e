package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/butlletinotes
 */

public class ButlletiNotes {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int nota = sc.nextInt();
		
		if (nota <=4) {
			System.out.println("Suspes");
			
		}else if (nota <= 6) {
			System.out.println("Aprovat");
			
		}else if (nota <= 8) {
			System.out.println("Notable");
			
		}else if (nota <= 10) {
			System.out.println("Excelent");
		}
	}

}
